package rw.ac.rca.wsexam.repository;

import rw.ac.rca.wsexam.bean.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISupplierRepository extends JpaRepository<Supplier, Integer>{

}
