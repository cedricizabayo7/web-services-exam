package rw.ac.rca.wsexam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rw.ac.rca.wsexam.bean.Item;
import rw.ac.rca.wsexam.bean.Supplier;

@Repository
public interface IItemRepository extends JpaRepository<Item, Integer>{

}
