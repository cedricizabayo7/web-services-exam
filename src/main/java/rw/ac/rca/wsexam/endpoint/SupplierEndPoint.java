package rw.ac.rca.wsexam.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rw.ac.rca.cedric.*;
import rw.ac.rca.wsexam.bean.Supplier;
import rw.ac.rca.wsexam.repository.ISupplierRepository;

import java.util.List;
import java.util.Optional;

@Endpoint
public class SupplierEndPoint {

	@Autowired
	private ISupplierRepository supplierRepository;
// operations(save,getAll,getById,delete and update)

	@PayloadRoot(namespace = "http://rca.ac.rw/cedric", localPart = "GetSupplierByIdRequest")
	@ResponsePayload
	public GetSupplierByIdResponse findById(@RequestPayload GetSupplierByIdRequest request) {

		Supplier supplier = supplierRepository.findById(request.getId()).get();

		GetSupplierByIdResponse response = mapSupplierResponse(supplier);
		return response;
	}

	@PayloadRoot(namespace = "http://rca.ac.rw/cedric", localPart = "GetAllSuppliersRequest")
	@ResponsePayload
	public GetAllSuppliersResponse findAll(@RequestPayload GetAllSuppliersRequest request) {

		GetAllSuppliersResponse allSuppliersResponse = new GetAllSuppliersResponse();
		List<Supplier> suppliers = supplierRepository.findAll();
		for (Supplier supplier : suppliers) {
			GetSupplierByIdResponse response = mapSupplierResponse(supplier);
            allSuppliersResponse.getSuppliers().add(response.getSupplier());
		}

		return allSuppliersResponse;
	}

	@PayloadRoot(namespace = "http://rca.ac.rw/cedric", localPart = "CreateSupplierRequest")
	@ResponsePayload
	public CreateSupplierResponse createNew(@RequestPayload CreateSupplierRequest request) {

		Supplier supplier = supplierRepository.save(new Supplier(request.getSupplier().getNames(), request.getSupplier().getEmail(), request.getSupplier().getMobile()));

		CreateSupplierResponse createSupplierResponse = mapCreateSupplier(supplier);
		return createSupplierResponse;
	}

	@PayloadRoot(namespace = "http://rca.ac.rw/cedric", localPart = "UpdateSupplierRequest")
	@ResponsePayload
	public UpdateSupplierResponse findById(@RequestPayload UpdateSupplierRequest request) {

		Optional<Supplier> supplier = supplierRepository.findById(request.getSupplier().getId());

		if(supplier.isPresent()){

			Supplier supplier1 = new Supplier(request.getSupplier().getId(),request.getSupplier().getNames(), request.getSupplier().getEmail(), request.getSupplier().getMobile());

			supplierRepository.save(supplier1);

			UpdateSupplierResponse response = mapUpdateSupplierResponse(supplier1);
			return response;
		}

		return null;
	}

	@PayloadRoot(namespace = "http://rca.ac.rw/cedric", localPart = "DeleteSupplierRequest")
	@ResponsePayload
	public DeleteSupplierResponse deleteOne(@RequestPayload DeleteSupplierRequest request) {

		supplierRepository.deleteById(request.getId());

		DeleteSupplierResponse deleteSupplierRequest = new DeleteSupplierResponse();
		deleteSupplierRequest.setMessage("Supplier successfully deleted");
		return deleteSupplierRequest;
	}

	private GetSupplierByIdResponse mapSupplierResponse(Supplier supplier) {
		rw.ac.rca.cedric.Supplier supplier1 = mapSupplier(supplier);

		GetSupplierByIdResponse response = new GetSupplierByIdResponse();

		response.setSupplier(supplier1);
		return response;
	}
	private UpdateSupplierResponse mapUpdateSupplierResponse(Supplier supplier) {
		rw.ac.rca.cedric.Supplier supplier1 = mapSupplier(supplier);

		UpdateSupplierResponse response = new UpdateSupplierResponse();

		response.setSupplier(supplier1);
		return response;
	}

	private CreateSupplierResponse mapCreateSupplier(Supplier supplier) {
		rw.ac.rca.cedric.Supplier supplier1 = mapSupplier(supplier);

		CreateSupplierResponse response = new CreateSupplierResponse();

		response.setSupplier(supplier1);
		return response;
	}

	private rw.ac.rca.cedric.Supplier mapSupplier(Supplier supplier) {
		rw.ac.rca.cedric.Supplier supplier1 = new rw.ac.rca.cedric.Supplier();
        supplier1.setEmail(supplier.getEmail());
        supplier1.setId(supplier.getId());
        supplier1.setMobile(supplier.getMobile());
        supplier1.setNames(supplier.getNames());
		return supplier1;
	}

}
