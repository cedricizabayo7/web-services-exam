package rw.ac.rca.wsexam.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rw.ac.rca.cedric.*;
import rw.ac.rca.wsexam.bean.Item;
import rw.ac.rca.wsexam.enumeration.StatusType;
import rw.ac.rca.wsexam.repository.IItemRepository;

import java.util.List;
import java.util.Optional;

@Endpoint
public class ItemEndPoint {
	@Autowired
	private IItemRepository iItemRepository;



	@PayloadRoot(namespace = "http://rca.ac.rw/cedric", localPart = "GetItemRequest")
	@ResponsePayload
	public GetItemResponse findById(@RequestPayload GetItemRequest request) {

		Item item = iItemRepository.findById(request.getId()).get();
		GetItemResponse getItemResponse = new GetItemResponse();
		getItemResponse.setItems(mapItems(item));
		return getItemResponse;
	}

	@PayloadRoot(namespace = "http://rca.ac.rw/cedric", localPart = "GetAllItemsRequest")
	@ResponsePayload
	public GetAllItemsResponse findAll(@RequestPayload GetAllItemsRequest request) {

		GetAllItemsResponse allItemsResponse = new GetAllItemsResponse();
		List<Item> items = iItemRepository.findAll();
		for (Item found_item : items) {
			GetItemResponse getItemResponse = new GetItemResponse();
			getItemResponse.setItems(mapItems(found_item));
			allItemsResponse.getItems().add(getItemResponse.getItems());
		}

		return allItemsResponse;
	}
	@PayloadRoot(namespace = "http://rca.ac.rw/cedric", localPart = "CreateItemRequest")
	@ResponsePayload
	public CreateItemResponse save(@RequestPayload CreateItemRequest request) {
		StatusType statusType = StatusType.valueOf(request.getItems().getStatus());
		iItemRepository.save(new Item(
				request.getItems().getName(),
				request.getItems().getItemCode(),
				request.getItems().getPrice(),
				statusType
		));
		CreateItemResponse createItemResponse = new CreateItemResponse();
		createItemResponse.setItems(request.getItems());
		createItemResponse.setMessage("Created Successfully");
		return createItemResponse;
	}

	@PayloadRoot(namespace = "http://rca.ac.rw/cedric", localPart = "UpdateItemRequest")
	@ResponsePayload
	public UpdateItemResponse update(@RequestPayload UpdateItemRequest request) {
		UpdateItemResponse updateItemResponse = new UpdateItemResponse();
		Optional<Item> existingItem = this.iItemRepository.findById(request.getItems().getId());
		if(existingItem.isEmpty() || existingItem == null) {
			updateItemResponse.setItems(null);
			updateItemResponse.setMessage("Id not found");
		}
		if(existingItem.isPresent()) {

			Item _item = existingItem.get();
			StatusType statusType = StatusType.valueOf(request.getItems().getStatus());
			_item.setName(request.getItems().getName());
			_item.setItemCode(request.getItems().getItemCode());
			_item.setPrice(request.getItems().getPrice());
			_item.setStatus(statusType);
			iItemRepository.save(_item);
			updateItemResponse.setItems(mapItems(_item));
			updateItemResponse.setMessage("Updated successfully");
		}
		return updateItemResponse;
	}

	@PayloadRoot(namespace = "http://rca.ac.rw/cedric", localPart = "DeleteItemRequest")
	@ResponsePayload
	public DeleteItemResponse save(@RequestPayload DeleteItemRequest request) {

		iItemRepository.deleteById(request.getId());

		DeleteItemResponse deleteItemResponse = new DeleteItemResponse();
		deleteItemResponse.setMessage("Deleted Successfully");
		return deleteItemResponse;
	}

	private Items mapItems(Item item) {
		Items itemDetails = new Items();
		itemDetails.setId(item.getId());
		itemDetails.setName(item.getName());
		itemDetails.setPrice(item.getPrice());
		itemDetails.setStatus(item.getStatus().toString());
		return itemDetails;
	}
}