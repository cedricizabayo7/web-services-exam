package rw.ac.rca.wsexam.enumeration;

public enum StatusType {
    NEW, GOOD_SHAPE, OLD
}
