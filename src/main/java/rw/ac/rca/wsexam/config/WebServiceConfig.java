package rw.ac.rca.wsexam.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

//Enable Spring Web Services
@EnableWs
//Spring Configuration
@Configuration
public class WebServiceConfig{

		@Bean
		public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
			MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
			messageDispatcherServlet.setApplicationContext(context);
			messageDispatcherServlet.setTransformWsdlLocations(true);
			return new ServletRegistrationBean(messageDispatcherServlet, "/ws/cedric/*");
		}

		@Bean(name = "supplier")
		public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema supplierSchema) {
			DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
			definition.setPortTypeName("SupplierPort");
			definition.setTargetNamespace("http://rca.ac.rw/cedric");
			definition.setLocationUri("/ws/cedric/suppliers");
			definition.setSchema(supplierSchema);
			return definition;
		}

	@Bean(name = "item")
	public DefaultWsdl11Definition defaultWsdl11Definition1(XsdSchema itemSchema) {
		DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
		definition.setPortTypeName("ItemPort");
		definition.setTargetNamespace("http://rca.ac.rw/cedric");
		definition.setLocationUri("/ws/cedric/items");
		definition.setSchema(itemSchema);
		return definition;
	}

		@Bean
		public XsdSchema supplierSchema() {
			return new SimpleXsdSchema(new ClassPathResource("supplier.xsd"));
		}

	@Bean
	public XsdSchema itemSchema() {
		return new SimpleXsdSchema(new ClassPathResource("item.xsd"));
	}
}
