package rw.ac.rca.wsexam.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Supplier {

	@Id
	@GeneratedValue
	private int id;
	
	private String names;
	
	private String email;

	private String mobile;

	public Supplier(int id, String names, String email, String mobile) {
		this.id = id;
		this.names = names;
		this.email = email;
		this.mobile = mobile;
	}

	public Supplier(String names, String email, String mobile) {
		super();
		this.names = names;
		this.email = email;
		this.mobile = mobile;
	}

	public Supplier() {
		super();
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Override
	public String toString() {
		return String.format("Supplier [id=%s, names=%s, email=%s, mobile=%s]", id, names, email, mobile);
	}

}
