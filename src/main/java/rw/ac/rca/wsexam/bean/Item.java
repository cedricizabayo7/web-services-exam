package rw.ac.rca.wsexam.bean;

import rw.ac.rca.wsexam.enumeration.StatusType;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.PrintConversionEvent;

@Entity
public class Item {

	@Id
	@GeneratedValue
	private int id;

	private String name;
	private String itemCode;
	private int price;
	private StatusType status;

	public Item() {}

	public Item(int id, String name, String itemCode, int price, StatusType status) {
		super();
		this.id = id;
		this.name = name;
		this.itemCode = itemCode;
		this.price = price;
		this.status = status;
	}


	public Item(String name, String itemCode, int price, StatusType status) {
		super();
		this.name = name;
		this.itemCode = itemCode;
		this.price = price;
		this.status = status;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public StatusType getStatus() {
		return status;
	}
	public void setStatus(StatusType status) {
		this.status = status;
	}
}